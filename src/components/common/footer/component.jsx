import React, { Component } from 'react';
import { Row, Col } from 'mdbreact';
import './footer.css'


export default class Footer extends Component {
  render() {
    return (
      <div className="foot">
        <div className="icons font-large white-text d-flex justify-content-center">
          <i class="fab fa-facebook-square"></i>&nbsp;<i class="fab fa-twitter"></i>&nbsp;
      <i class="fab fa-instagram"></i>&nbsp;<i class="fab fa-linkedin"></i>
        </div>
        <div className="write">
          <Row>
            <Col md="6" className="rightBroder">
              <ul>
                <li><h1>Information sheet</h1></li>
                <li>manstenganse updated with our new openings,next,special season and promocious.</li>
              </ul>
              
             <input
                className="subEmail"
                onChange={this.getInput}
                value={'email'}
                type={'email'}
                placeholder={'email'}
             /><button>Enivar</button>
            </Col>
            <Col md="6">
            <div className="text">
              <ul>
                <li><i class="fas fa-map-marker-alt"></i> Calle Bergueda 43 local 10,08211 castellar del valles ,
                barcelona(Spain)</li><br></br>
                <li><i class="fas fa-mobile-alt"></i> (+34)676 639 005 (+34)937 976 152 </li><br></br>
                <li><i class="fas fa-envelope-square"></i> info@3digitalfactory.com</li>

              </ul>
              </div>
            </Col>
          </Row>
        </div>
        <Row>
          <Row >
        <div className="last d-inline-flex justify-content-center ">
          <ul>
            <li>IMPRESSION 3D</li>
            <li>TEAM</li>
            <li>CONTACT</li>
            <li>ABOUT US</li>
          </ul>
        </div></Row>
        <Row>
        <div className="lastline d-inline-flex justify-content-center ">
        <ul>
          <li> Legal warning</li>
          <li>Politices of privacity</li>
          <li>Coolies policy</li>
          <li>Envious and Devoluciounes policy </li>
        </ul>
        </div></Row>
        </Row>
      </div>
    )
  }
}
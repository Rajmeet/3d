import React, {Component} from 'react';
import { Row, Col } from 'mdbreact';
import Logo from '../../../../assets/icons/logoGreen.png';
import './curveheader.css';
 
class Curveheading extends Component {
  render () {
    return (
  <div className="curve">
    <Row >
      <Col lg="2">
        <img className="logo" alt="logo" src={Logo} />
      </Col>
      <Col lg="6">
        <div className="box d-inline-flex justify-content-center ">
          <ul>
            <li>START</li>
            <li>TECHNOLOGY</li>
            <li>WE</li>
            <li>CONTACT</li>
            <li>BLOG</li>
          </ul>
        </div>
      </Col>
      <Col lg="4">
        <div className="contact">
          <ul>
            <li><i class="fas fa-envelope-square"></i> info@3digitalfactory.com</li><br></br>
            <li><i class="fas fa-mobile-alt"></i> +1 39402-89403</li>
          </ul>

          <button onclick="text" className="reg " >Login</button>
          <button onclick="text" className="reg ml-4">Register</button>

        </div>

      </Col>

    </Row>
  </div>
    )
}
}
export default Curveheading
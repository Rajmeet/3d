import React,{Component} from 'react';
import { BrowserRouter,Switch,Route } from 'react-router-dom';
import Footer from './components/common/footer/component';
import Landingheader from './components/common/header/landingheader/component';
import Curveheader from './components/common/header/curveheader/component';

class App extends Component {
  render() {

    return (
      <BrowserRouter>
        <Switch>       
          <Route path='/footer' component={Footer}/>
          <Route path='/landingheader' component={Landingheader}/>
          <Route path='/curveheader' component={Curveheader}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;

